import mongoose from 'mongoose';
import uriBuilder from "mongo-uri-builder";

export const connect = async () => {
  try {
    const connectionString = uriBuilder({
      host: process.env.DB_HOST,
      port: process.env.DB_PORT || '27017',
      database: 'gyra'
    })
    await mongoose.connect(connectionString, {
      useUnifiedTopology: true,
      useNewUrlParser: true
    })
  } catch (error) {
    throw error;
  }
}