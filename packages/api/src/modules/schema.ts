import { GraphQLSchema, GraphQLObjectType } from "graphql";
import MessageQueries from "@modules/messages/MessagesQueries";
import MessageMutations from "@modules/messages/MessagesMutations";
import MessagesSubscriptions from "@modules/messages/MessagesSubscriptions";

const schema = new GraphQLSchema({
  query: new GraphQLObjectType({
    name: "Query",
    fields: () => ({
      ...MessageQueries,
    }),
  }),
  mutation: new GraphQLObjectType({
    name: "Mutation",
    fields: () => ({
      ...MessageMutations,
    }),
  }),
  subscription: new GraphQLObjectType({
    name: 'Subscription',
    fields: () => ({
      ...MessagesSubscriptions
    })
  })
});

export default schema;
