import {
  GraphQLID,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLString,
} from "graphql";

import { DateTimeResolver, ObjectIDResolver} from 'graphql-scalars'

const Message = new GraphQLObjectType({
  name: "Message",
  description: "Definitions of chat messages",
  fields: () => ({
    _id: {
      type: ObjectIDResolver,
    },
    content: {
      type: GraphQLString,
      resolve: (message) => message.content,
    },
    createdBy: {
      type: GraphQLString,
      resolve: (message) => message.createdBy,
    },
    createdAt: {
      type: DateTimeResolver,
      resolve: (message) => message.createdAt,
    },
    updatedAt: {
      type: DateTimeResolver,
      resolve: (message) => message.updatedAt
    },
  }),
});

export default Message;
