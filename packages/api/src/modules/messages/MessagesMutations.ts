import CreateMessageInput from "./input/CreateMessageInput";
import MessageModel from "./MessageModel";
import Message from "./MessageType";
import pubSub from '../pubSub'

const chatMessages = {
  createMessage: {
    type: Message,
    args: {
      input: {
        type: CreateMessageInput,
      },
    },
    resolve: async (_, { input: { content, createdBy, chat } }, ctx) => {
      const resultMessage = await MessageModel.create({
        content,
        createdBy,
      });
      
      ctx.pubSub.publish("NEW_MESSAGE", {
        newMessage: { content, createdBy, chat },
      });
      return resultMessage;
    },
  },
};

export default chatMessages;
