import mongoose, { Document, Model } from "mongoose";

const schema = new mongoose.Schema(
  {
    content: {
      type: String,
    },
    createdBy: {
      type: String
    }
  },
  {
    timestamps: {
      createdAt: "createdAt",
    },
  }
);

export interface IMessage extends Document {
  content: string;
  createdBy: string;
}

const MessageModel: Model<IMessage> = mongoose.model("Message", schema);

export default MessageModel;
