import { GraphQLString, GraphQLInputObjectType, GraphQLNonNull } from "graphql";

const CreateMessageInput = new GraphQLInputObjectType({
  name: "CreateMessageInput",
  description: "Input payload for creating message",
  fields: () => ({
    content: {
      type: new GraphQLNonNull(GraphQLString),
    },
    createdBy: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default CreateMessageInput;
