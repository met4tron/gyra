import { withFilter } from "apollo-server";
import CreateMessageInput from "./input/CreateMessageInput";
import MessageModel from "./MessageModel";
import Message from "./MessageType";
import pubSub from "../pubSub";

const chatMessages = {
  newMessage: {
    type: Message,
    subscribe: withFilter(
      () => pubSub.asyncIterator("NEW_MESSAGE"),
      (payload, variables) => {
        console.log(1);
        return true;
      }
    ),
  },
};

export default chatMessages;
