import { GraphQLList, GraphQLNonNull, GraphQLString } from "graphql";
import MessageModel from './MessageModel';
import Message from "./MessageType";

const UserQueries = {
  messages: {
    type: GraphQLList(Message),
    args: {
      chat: {
        type: GraphQLString,
      },
    },
    resolve: async (_source, { chat }) => {
      try {
        const messages = await MessageModel.find({
          chat
        }).sort({ createdAt: 1 }).lean();
        console.log(messages)
        return messages || []
      } catch (error) {
        console.log(error);
      }
    },
  },
};

export default UserQueries;
