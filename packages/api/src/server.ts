import "dotenv/config";

import express from "express";
import cors from "cors";
import helmet from "helmet";
import http from 'http';
import { ApolloServer } from "apollo-server-express";
import { connect } from "@config/database";
import schema from '@modules/schema'
import pubSub from '@modules/pubSub'

const app = express();

app.use(cors());
app.use(
  helmet({
    contentSecurityPolicy:
      process.env.NODE_ENV === "production" ? undefined : false,
  })
);

const server = new ApolloServer({
  schema,
  playground: process.env.NODE_ENV !== 'production',
  context: (req) => ({ req, pubSub })
})

server.applyMiddleware({
  app,
  path: '/graphql'
});

const httpServer = http.createServer(app);

server.installSubscriptionHandlers(httpServer);

connect()
  .then((_) => {
    httpServer.listen(process.env.PORT, () => {
      console.log(`Yayy!!! API is running on PORT ${process.env.PORT}`);
    });
  })
  .catch((err) => console.log(err));
